const WebSocket = require('ws')

let wss

module.exports = function (server) {
  if (!wss) {
    wss = new WebSocket.Server({ server })
  }
  return wss
}

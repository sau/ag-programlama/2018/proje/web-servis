const express = require('express')
const morgan = require('morgan')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')

const app = express()
const server = require('http').createServer(app)
const wss = require('./wss')(server)

app.use(morgan('combined'))
app.use(bodyParser.json({ limit: '50mb' }))

mongoose
  .connect('mongodb://localhost:27017/news', { useNewUrlParser: true })
  .catch(reason => console.error(reason))

require('./models/news')
require('./models/news_types')
app.use(require('./routes'))

wss.on('connection', ws => {
  ws.on('message', message => {
    console.log(`Received: ${message}`)
  })

  ws.send(`Welcome`)
})

server.listen(process.env.PORT || 3000, () => {
  console.log(`Server started on port ${server.address().port}`)
})
